<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Contact;
use App\Carrier;
use App\Mobile;
use App\Call;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function valid_contact()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);
		$name = "John Smith";

		$response = $mobile->makeCallByName($name);

		$this->assertInstanceOf(Call::class, $response);
	}

	/** @test */
	public function contact_not_found()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);
		$name = "John ABC";

		$mobile->makeCallByName($name);

		$this->expectExceptionMessage('Invalid contact');
	}
}
