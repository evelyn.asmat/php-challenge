<?php

namespace App;


class Contact
{
	protected $name;
	protected $number;
	
	function __construct(string $name, string $number)
	{
		$this->name = $name;
		$this->number = $number;
	}
}