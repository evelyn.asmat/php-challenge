<?php

namespace App\Services;

use App\Contact;
use Exception;

class ContactService
{
	
	public static function findByName(): Contact
	{
		// queries to the db

		// example: contact value returned from db
		$name = 'John Smith';
		$number = '+51992838508';

		if (empty($name) && empty($number)){
			throw new Exception('Invalid Contact');
		}

		$contact = new Contact($name, $number);

		return $contact;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
	}
}