<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		try{
			$contact = ContactService::findByName($name);
		} catch (Exception $e) {
		    echo 'Exception: ',  $e->getMessage(), "\n";
		}

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSMSByNumber($number = '', $body = '')
	{
		if( empty($number) || empty($body) ) return;

		$is_valid = ContactService::validateNumber($number);

		if ($is_valid){
			return $this->provider->sendSMS();
		}
	}


}
