<?php

namespace App;

use App\Call;
use App\Interfaces\CarrierInterface;


class Carrier implements CarrierInterface
{
	
	public function dialContact(Contact $contact)
	{
		/* Actions */
	}

	public function makeCall(): Call
	{
		$call = new Call();
		return $call;
	}

	public function sendSMS(string $number, string $body)
	{
		/* Actions */
	}

}
